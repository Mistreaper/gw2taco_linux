How to contribute
=================

Thanks for taking an interest in this project. Here are a few ways you can help out.


Bugs
----

If you encounter any bugs or errors please create an issue and provide as much info as possible along with:

* **Terminal log**
* **GW2Taco.log**
* **settings.conf**
* **taco.links** if modified/related to error
* Steps to recreate the error

Enhancements/Feature Requests
------------------------------

If you have, or would like to request, a new feature or enhancement, please create an issue describing the details.

Code Contributions
----------------

In order to contribute code to the project please contact me. I am still new to Git & Open Source licensing.

From what I understand any code contributions made to the project
are copyrighted to the contributer. This may cause issues in the future or may not.., I don't know.

In the meantime we will simply ask you to hand over those copyrights to "the project", with the informal statement "I hereby confirm that I own the copyright in this code and assign the copyright to the project, to be licensed under the same terms as the rest of the code."

This is a sort of informal Contributer License Agreement. You will still be attributed for your contributions.

Please contact me if you understand Open Source projects and licenses, and have an idea of how the project should be licensed for the benefit of all those involved.